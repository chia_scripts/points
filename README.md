# Surveillance des points d'une ferme Chia intégrée à une pool
C'est mon premier Git, soyez indulgent.

Cela fonctionne sous Ubuntu et Debian. Pour Windows, il va vous falloir adapter tout cela.

Le code doit être largement améliorable mais il a le mérite d'être fonctionnel.

## Installation sous Linux (Ubuntu / Debian)
Vous devez :
1. installer [**gnuplot**](http://www.gnuplot.info/download.html)
2. copier et rendre éxécutable le fichier `script-points.sh`
3. créer une tâche **cron** (`crontab -e`) pour éxécuter régulièrement `script-points.sh` (10 minutes pour moi, `*/10	*	*	*	*	/home/nomdutilisateur/script-points.sh`)
4. copier le fichier `script-points.gnuplot`
5. créer une tâche **cron** pour éxécuter régulièrement `gnuplot /home/nomdutilisateur/script-points.gnuplot` (à 5 toutes les heures, `5	*	*	*	*	gnuplot /home/nomdutilisateur/script-points.gnuplot`

## Principe
Le script `script-points.sh` crée un fichier CSV contenant les données et le fichier `script-points.gnuplot` contient le formatage que doit utiliser **gnuplot** pour faire le graphique.

Bon courage.
