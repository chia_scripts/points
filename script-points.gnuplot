set datafile separator ';'
set key autotitle columnhead # use the first line as title
set terminal pngcairo size 2560,1024 enhanced font 'Segoe UI,10'
set object 1 rectangle from screen 0,0 to screen 1,1 fillcolor rgb"#cccccc" behind

set xdata time
set timefmt "%Y-%m-%d_%H:%M:%S"
#set format "%Y-%m-%d_%H:%M:%S"
set xlabel "Date"
#set xrange ["2021-08-05_20:00:00":]

set output 'chia-activite-points.png'

set ytics nomirror # tc lt 1
set ylabel "Nombre de points" # tc lt 1

set y2tics nomirror # tc lt 2
#set y2range [0:300]
set y2label "Nombre de plots" # tc lt 2

set title "Evolution des points en fonction du temps"

plot "chia-activite.csv" using 1:3 w l lw 3 axes x1y2 title "Nombre de plots", \
	"chia-activite.csv" using 1:2 w l lw 3 axes x1y1 title "Points"
