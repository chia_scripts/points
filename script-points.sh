#!/bin/bash
nom=$(hostname)
ladate=$(date +%F_%T)
chia="/usr/lib/chia-blockchain/resources/app.asar.unpacked/daemon/chia"
#Vous devez remplacer par votre fingerprint qui n'a pas forcément 9 chiffres
fingerprint=123456789
fichierCSV="chia-activite.csv"

tout=$($chia plotnft show -f $fingerprint)
#echo "DEBUG: tout: $tout"
points=$(echo "$tout" | awk '{if($1 == "Points"){print $NF}}')
syncpool=$(echo "$tout" | awk '{if($1 == "Sync"){print $NF}}')

tout=$($chia farm summary)
#echo "DEBUG: tout: $tout"
nbplots=$(echo "$tout" | awk '{if($1 == "Plot"){print $NF}}')
syncfarm=$(echo "$tout" | awk '{if($1 == "Farming"){print $NF}}')

if [ ! -f $fichierCSV ]
	then
	echo "date;points;plots;host;pool;farm" > $fichierCSV
fi

#echo "DEBUG: ladate:"$ladate";points:"$points";nbplots:"$nbplots";nom:$nom;syncpool:"$syncpool";syncfarm:"$syncfarm""
echo ""$ladate";"$points";"$nbplots";$nom;"$syncpool";"$syncfarm"" >> $fichierCSV
